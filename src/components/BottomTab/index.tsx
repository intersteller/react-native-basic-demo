import React from 'react';
import {BottomTabBar} from 'react-navigation-tabs';

import styles from './BottomTab.style';

const BottomTab = (props: any) => {
  return (
    <BottomTabBar
      {...props}
      activeTintColor="#e91e63"
      labelStyle={{fontSize: 16}}
      style={styles.container}
    />
  );
};

export default BottomTab;
