import {StyleSheet, ViewStyle, TextStyle} from 'react-native';

interface Styles {
  container: ViewStyle;
  title: TextStyle;
}

export default StyleSheet.create<Styles>({
  container: {
    paddingBottom: 12,
    backgroundColor: '#fce9a6',
    borderTopColor: '#605F60',
  },
  title: {
    color: '#000',
  },
});
