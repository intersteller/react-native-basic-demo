import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#039385',
    height: '100%',
  },
  title: {
    color: '#FFFFFF',
    fontSize: 30,
  },
});
