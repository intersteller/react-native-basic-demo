import React, {Component} from 'react';
import {Text, View, Button} from 'react-native';
import {NavigationScreenProp, NavigationState} from 'react-navigation';

import styles from './Page4.style';

interface Props {
  navigation: NavigationScreenProp<NavigationState>;
}

class Page4 extends Component<Props> {
  static navigationOptions = ({navigation}: any) => {
    return {
      title: 'Back to Page 3',
      headerStyle: {
        backgroundColor: '#25A565',
      },
      headerTintColor: '#FFF',
      headerTitleStyle: {
        fontWeight: '300',
      },
      headerRight: (
        <Button
          onPress={() => navigation.navigate('Page6')}
          title="Page 6"
          color="#000"
        />
      ),
    };
  };

  render() {
    return (
      <>
        <View style={styles.container}>
          <Text style={styles.title}>Page 4</Text>
        </View>
      </>
    );
  }
}

export default Page4;
