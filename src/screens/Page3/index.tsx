import React from 'react';
import {Text, Button, View} from 'react-native';
import {NavigationScreenProp, NavigationState} from 'react-navigation';

import styles from './Page3.style';

interface Props {
  navigation: NavigationScreenProp<NavigationState>;
}

const Page3 = ({navigation}: any) => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.title}>Page 3</Text>
        <Button
          title="Go to Page 4"
          onPress={() => navigation.navigate('Page4')}
        />
      </View>
    </>
  );
};

export default Page3;
