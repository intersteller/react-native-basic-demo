import React from 'react';
import {Text, View, Image} from 'react-native';

import styles from './Page2.style';

const Page2 = () => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.title}>Page 2</Text>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: 'https://reactnative.dev/img/tiny_logo.png',
          }}
        />
      </View>
    </>
  );
};

export default Page2;
