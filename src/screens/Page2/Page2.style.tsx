import {StyleSheet, ViewStyle, TextStyle, ImageStyle} from 'react-native';

interface Styles {
  container: ViewStyle;
  title: TextStyle;
  tinyLogo: ImageStyle;
}

export default StyleSheet.create<Styles>({
  container: {
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#8DC63F',
    height: '100%',
  },
  title: {
    marginBottom: 30,
    color: '#FFFFFF',
    fontSize: 30,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
});
