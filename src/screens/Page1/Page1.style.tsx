import {StyleSheet, ViewStyle, TextStyle} from 'react-native';

interface Styles {
  container: ViewStyle;
  title: TextStyle;
}

export default StyleSheet.create<Styles>({
  container: {
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DD441B',
    height: '100%',
  },
  title: {
    marginBottom: 30,
    color: '#FFFFFF',
    fontSize: 40,
    fontFamily: 'Lato-Light',
    fontWeight: '300',
  },
});
