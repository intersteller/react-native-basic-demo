import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import {NavigationScreenProp, NavigationState} from 'react-navigation';

import styles from './Page1.style';

interface Props {
  navigation: NavigationScreenProp<NavigationState>;
}

class Page1 extends Component<Props>{
  render() {
    return (
      <>
        <View style={styles.container}>
          <Text style={styles.title}>Page 1</Text>
          <Button
            title="Go to Page 5"
            onPress={() => this.props.navigation.navigate('Page5')}
          />
        </View>
      </>
    );
  }
};

export default Page1;
