import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1C95A5',
    height: '100%',
  },
  title: {
    marginBottom: 30,
    color: '#FFFFFF',
    fontSize: 30,
  },
});
