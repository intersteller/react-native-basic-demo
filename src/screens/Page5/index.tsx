import React from 'react';
import {TouchableOpacity, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './Page5.style';

const Page5 = () => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.title}>Page 5</Text>
        <Icon.Button
          name="facebook"
          backgroundColor="#3b5998"
          onPress={() => alert('Clicked on Facebook button')}
        >
          <Text style={{fontFamily: 'Arial', fontSize: 15}}>
            Login with Facebook
          </Text>
        </Icon.Button>
        <Icon.Button name="twitter" onPress={() => alert('Clicked on Twitter button')}>
          <Text style={{fontFamily: 'Arial', fontSize: 15}}>
            Login with Twitter
          </Text>
        </Icon.Button>
        <Icon name="comments" size={30} color="#FFF" />
        <TouchableOpacity accessible={true} accessibilityLabel="Tap me!">
          <View>
            <Text>Press me!</Text>
          </View>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default Page5;
