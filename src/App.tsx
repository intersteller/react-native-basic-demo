import React from 'react';
import {
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer,
} from 'react-navigation';
import {fromLeft} from 'react-navigation-transitions';

import Page1 from './screens/Page1';
import Page2 from './screens/Page2';
import Page3 from './screens/Page3';
import Page4 from './screens/Page4';
import Page5 from './screens/Page5';
import Page6 from './screens/Page6';

import BottomTab from './components/BottomTab';

const CombineNavigator = createStackNavigator(
  {
    Page3: {
      screen: Page3,
      navigationOptions: () => ({
        title: 'Page 3',
        headerBackTitle: null,
      }),
    },
    Page4: Page4,
  },
  {
    initialRouteName: 'Page3',
    transitionConfig: () => fromLeft(),
  },
);

const AppNavigator = createBottomTabNavigator(
  {
    'Page1': Page1,
    Page2: Page2,
    Combine: CombineNavigator,
    Page5: Page5,
    Page6: Page6,
  },
  {
    tabBarComponent: (props: any) => <BottomTab {...props} />,
  },
);

export default createAppContainer(AppNavigator);
